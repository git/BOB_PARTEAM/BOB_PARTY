import { StyleSheet } from "react-native";

export default StyleSheet.create({
    textID: {
        fontSize: 16,
        lineHeight: 21,
        letterSpacing: 0.25,
        color: 'white',
        textAlign: 'right',
        paddingVertical: 5,
    },
    text: {
        fontSize: 16,
        lineHeight: 21,
        letterSpacing: 0.25,
        color: 'white',
        paddingVertical: 5,
    },
    title: {
        fontSize: 20,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
        marginTop: 15,
    },
    infoView: {
        borderColor: '#2D2C33',
         borderWidth: 2,
        width: '90%',
        margin: 15,
        padding: 15,
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30,
        borderRadius: 10,
        backgroundColor: '#888898',
    },
    buttonText: {
        fontSize: 20,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
    },
});