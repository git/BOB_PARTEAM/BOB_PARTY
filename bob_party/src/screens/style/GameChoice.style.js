import { StyleSheet } from "react-native";

export default StyleSheet.create({
    text :{
        padding: 10,
        fontSize: 23,
        fontFamily: 'Helvetica',
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
    },
    textInput: {
        backgroundColor: 'white',
        flex: 1,
        margin: '2%',
        paddingLeft: 10,
        borderRadius: 15,
      }
  });